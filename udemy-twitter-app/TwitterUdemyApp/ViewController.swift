//
//  ViewController.swift
//  TwitterUdemyApp
//
//  Created by Kerim Çağlar on 29/04/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var signupButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //Butona ovallik verme-corder radius
        signupButton.layer.cornerRadius = 8
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

