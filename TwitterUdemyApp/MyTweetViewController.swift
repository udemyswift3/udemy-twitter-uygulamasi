//
//  MyTweetViewController.swift
//  TwitterUdemyApp
//
//  Created by Kerim Çağlar on 20/06/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import UIKit
import Firebase

class MyTweetViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var ref = FIRDatabase.database().reference()
    var user = FIRAuth.auth()?.currentUser
    
    // MVC MVVM Design Pattern
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tweetsTableView: UITableView!
    
    var tweets = [String]()
    var dates = [String]()
    var userInfo = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ref.child("users").child(user!.uid).observeSingleEvent(of: .value, with: { (snapshot) in
            let userValue = snapshot.value as? NSDictionary
            let name = userValue?["fullName"]
            let mentionName = userValue?["mentionName"]
            
            self.userInfo.append(name as! String)
            self.userInfo.append(mentionName as! String)
            
            self.ref.child("tweets/\(self.user!.uid)").observe(.childAdded, with: { (snapshot) in
                
                let value = snapshot.value as? NSDictionary
                let tweet = value?["tweet"]
                let tarih = value?["tarih"]
                
                self.tweets.append(tweet! as! String)
                self.dates.append(tarih! as! String)
                
                self.tweetsTableView.insertRows(at: [IndexPath(row:self.tweets.count-1,section:0)], with: UITableViewRowAnimation.automatic)
                
            })
        }) { (error) in
            
            print(error.localizedDescription)
        }

    }
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return tweets.count
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myTweets", for: indexPath)
        
        cell.textLabel?.font = UIFont.systemFont(ofSize: 12)
        cell.textLabel?.text = tweets[indexPath.row]
        
        return cell
    }
}
