//
//  SingleTweetTableViewCell.swift
//  TwitterUdemyApp
//
//  Created by Taha Sönmez on 03/06/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import UIKit

class SingleTweetTableViewCell: UITableViewCell {

    @IBOutlet var profilePicture: UIImageView!
    
    @IBOutlet var name: UILabel!
    
    @IBOutlet var mentionName: UILabel!
    
    @IBOutlet var tweet: UITextView!
    
    
    
    @IBOutlet var retweetButton: UIButton!
    
    @IBOutlet var numberOfLikers: UILabel!
    
    @IBOutlet var numberOfRetweeters: UILabel!
    
    @IBOutlet var numberOfRepliers: UILabel!
    
    @IBAction func replyButtonAction(_ sender: Any) {
    }
    
    @IBAction func retweetButtonAction(_ sender: Any) {
    }
    
    @IBAction func likeButtonAction(_ sender: Any) {
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func confic(profilePicture:String?, name:String, mentionName:String){
        self.mentionName.text = "@"+mentionName
        self.name.text = name
        
        if(profilePicture != nil)
        {
            let imageData = NSData(contentsOf: NSURL(string:profilePicture!)! as URL)
            self.profilePicture.image = UIImage(data: imageData! as Data)
        }
        
        else{
            self.profilePicture.image = UIImage(named: "egg")
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
