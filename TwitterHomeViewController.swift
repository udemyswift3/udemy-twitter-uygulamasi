//
//  TwitterHomeViewController.swift
//  TwitterUdemyApp
//
//  Created by Kerim Çağlar on 27/05/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import UIKit
import Firebase

class TwitterHomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var ref = FIRDatabase.database().reference()
    var user = FIRAuth.auth()?.currentUser
    
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var TweetsTableView: UITableView!
    
    var tweets = [String]()
    var dates = [String]()
    var userInfo = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref.child("users").child(user!.uid).observeSingleEvent(of: .value, with: { (snapshot) in

            let userValue = snapshot.value as? NSDictionary
            let name = userValue?["fullName"]
            let mentionName = userValue?["mentionName"]
            
            self.userInfo.append(name as! String)
            self.userInfo.append(mentionName as! String)
            
            self.ref.child("tweets/\(self.user!.uid)").observe(.childAdded, with: { (snapshot) in
                
                let value = snapshot.value as? NSDictionary
                let tweet = value?["tweet"]
                let tarih = value?["tarih"]
                
                self.tweets.append(tweet! as! String)
                self.dates.append(tarih! as! String)
                
                self.TweetsTableView.insertRows(at: [IndexPath(row:self.tweets.count-1,section:0)], with: UITableViewRowAnimation.automatic)
                
                self.loading.stopAnimating()
            })
            
            
        }) { (error) in
            
            print(error.localizedDescription)
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.tweets.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "tweetCell", for: indexPath) as! SingleTweetTableViewCell
        
        cell.tweet.isEditable = false
        
        let imageView = cell.profilePicture!
        imageView.layer.cornerRadius = 5.0
        imageView.layer.masksToBounds = true
        
        print(userInfo)
        
        self.ref.child("users").child(user!.uid).observe(.value, with: { (snapshot) in
            
            let snapshot = snapshot.value as! NSDictionary
            if (snapshot["profilePicture"] != nil){
                let profilePic = snapshot["profilePicture"] as! String
                let data = try? Data(contentsOf: URL(string:profilePic)!)
                self.profilResmiAyarlama(cell.profilePicture, imageToSet: UIImage(data:data!)!)
            }
        })
        
        if(tweets.count > 0){
            cell.tweet.text = tweets[indexPath.row]

        }
        else{
            
            cell.tweet.text = "Henüz Girilen tweet yoktur"
        }
        
        //cell.name.text = userInfo[0]
        //cell.mentionName.text = userInfo[1]
        
        cell.confic(profilePicture: nil, name: self.userInfo[0], mentionName: self.userInfo[1])
        
        return cell
    }

    func profilResmiAyarlama(_ imageView:UIImageView, imageToSet:UIImage){
        
        imageView.image = imageToSet
    }
}
