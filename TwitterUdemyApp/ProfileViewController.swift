//
//  ProfileViewController.swift
//  TwitterUdemyApp
//
//  Created by Kerim Çağlar on 20/06/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import UIKit
import Firebase

class ProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    @IBOutlet weak var tweetContainer: UIView!
    @IBOutlet weak var begeniContainer: UIView!
    @IBOutlet weak var mediaContainer: UIView!
    @IBOutlet weak var profilePicture: UIImageView!
    
    @IBOutlet weak var fullName: UILabel!
    @IBOutlet weak var mentionName: UILabel!
    @IBOutlet weak var about: UILabel!
    
    var user = FIRAuth.auth()?.currentUser
    var userData:NSDictionary?
    
    var ref = FIRDatabase.database().reference()
    var storageRef = FIRStorage.storage().reference()
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Profil resmi ayarları
        profilePicture.layer.cornerRadius = 5
        profilePicture.layer.borderColor = UIColor.white.cgColor
        profilePicture.layer.masksToBounds = true
        
        //Kullanıcı işlemleri
        self.ref.child("users").child(user!.uid).observe(.value, with: { (snapshot) in
            
            print(snapshot)
            
            let snapshot = snapshot.value as! NSDictionary
            
            self.userData = snapshot
            
            self.fullName.text = snapshot["fullName"] as? String
            self.mentionName.text = "@\(snapshot["mentionName"] as? String ?? "")"
            self.about.text = snapshot["about"] as? String
            
            if (snapshot["profilePicture"] != nil ) {
                
                let profilePic = snapshot["profilePicture"] as! String
                let data = try? Data(contentsOf: URL(string:profilePic)!)
                
                self.profilResmiAyarlama(self.profilePicture, imageToSet: UIImage(data:data!)!)
            }
            
        })
    }
    
    @IBAction func icerikGoruntule(_ sender: UISegmentedControl) {
        
        
        if(sender.selectedSegmentIndex == 2)
        {
            UIView.animate(withDuration: 0.2, animations: {
                
                self.tweetContainer.alpha = 1 // Görünür
                self.mediaContainer.alpha = 0 // Görünmez
                self.begeniContainer.alpha = 0 // Görünmez
            })
        }
        else if(sender.selectedSegmentIndex == 1)
        {
            UIView.animate(withDuration: 0.2, animations: {
                
                self.mediaContainer.alpha = 1 // sadece media görünür
                self.tweetContainer.alpha = 0 // görünmez
                self.begeniContainer.alpha = 0 // görünmez
                
            })
        }
        else
        {
            UIView.animate(withDuration: 0.2, animations: {
                self.begeniContainer.alpha = 1 // Görünür
                self.tweetContainer.alpha = 0 //görünmez
                self.mediaContainer.alpha = 0 // görünmez
            })
        }
    }
    
    @IBAction func ayarlar(_ sender: Any) {
        
        let actionSheet = UIAlertController(title:"Profil Resmi", message:"Profil Resminizi Seçiniz", preferredStyle:.actionSheet)
        
        let resimGalerisi = UIAlertAction(title: "Resimler", style: .default) { (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum)
            {
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
                self.imagePicker.allowsEditing = true
                self.present(self.imagePicker, animated:true, completion:nil)
            }
        }
        
        let kamera = UIAlertAction(title: "Kamera", style: .default) { (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
            {
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                self.imagePicker.allowsEditing = true
                self.present(self.imagePicker, animated:true, completion:nil)
            }
        }
        
        actionSheet.addAction(resimGalerisi)
        actionSheet.addAction(kamera)
        
        actionSheet.addAction(UIAlertAction(title:"İptal", style:.cancel, handler:nil))
        
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        
        profilResmiAyarlama(self.profilePicture, imageToSet: image)
        
        if let imageData : Data = UIImagePNGRepresentation(self.profilePicture.image!){
            
            let profilePicStorageRef = storageRef.child("users/\(String(describing: user!.uid))/profilePicture")
            
            let _ = profilePicStorageRef.put(imageData, metadata: nil)
            {metadata,error in
                
                if(error == nil)
                {
                    let downloadUrl = metadata!.downloadURL()
                self.ref.child("users").child(self.user!.uid).child("profilePicture").setValue(downloadUrl!.absoluteString)
                    
                }
                else
                {
                    print("error")
                }
                
            }
            
        }
        
        self.dismiss(animated: true, completion: nil)
    }

    
    @IBAction func cikisYap(_ sender: Any) {
        try! FIRAuth.auth()?.signOut()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let hosgeldinVC = storyboard.instantiateViewController(withIdentifier: "HosGeldinVC") as! ViewController
        
        self.present(hosgeldinVC, animated: true, completion: nil)
        
        UserDefaults.standard.removeObject(forKey: "email")
         UserDefaults.standard.removeObject(forKey: "password")
        
            }
    
    func profilResmiAyarlama(_ imageView:UIImageView, imageToSet:UIImage){
        
        imageView.image = imageToSet
    }

}
