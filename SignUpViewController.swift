//
//  SignUpViewController.swift
//  TwitterUdemyApp
//
//  Created by Kerim Çağlar on 06/05/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import UIKit
import Firebase

class SignUpViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var emailField: UITextField!
    
    @IBOutlet weak var passField: UITextField!
    
    @IBOutlet weak var passAgainField: UITextField!
    
    var ref = FIRDatabase.database().reference()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        emailField.delegate = self
        passField.delegate = self
        passAgainField.delegate = self
        
        // Do any additional setup after loading the view.
    }


    @IBAction func SignUp(_ sender: Any) {
        
        if(passField.text != passAgainField.text)
        {
            print("Şifre Tekrarları uyuşmuyor")
            showAlert(title: "", message: "Şifre tekrarları uyuşmuyor")
        }
        else if(!(emailField.text?.isEmpty)! && !(passField.text?.isEmpty)! && !(passAgainField.text?.isEmpty)!)
        {
            FIRAuth.auth()?.createUser(withEmail: emailField.text!, password: passField.text!, completion: { (user, error) in
                
                
                if(error != nil) // HATA BOŞ DEĞİLSE
                {
                    if let hataKodu = FIRAuthErrorCode(rawValue: error!._code) {
                        
                        switch hataKodu {
                        case .errorCodeInvalidEmail:
                            print("Geçersiz Email")
                            self.showAlert(title: "", message: "Geçersiz bir Email girdiniz.")
                        case .errorCodeEmailAlreadyInUse:
                            print("Bu mail adresi zaten kullanımda.")
                            self.showAlert(title: "", message: "Bu Email adresi zaten kullanımda.")
                        case .errorCodeWeakPassword:
                            self.showAlert(title: "", message: "Belirlediğiniz şifre çok zayıf.")
                            print("Daha güçlü bir şifre kullanın.")
                        case .errorCodeNetworkError:
                            self.showAlert(title: "", message: "İnternet bağlantınızda bir hata saptandı.")
                        // Yukarıdakilerin dışındaki bir hata için yapılacak default işlem; 
                        default:
                            print("Bir hata oluştu: \(error!)")
                            
                            self.showAlert(title: "", message: "Bir hata oluştu daha sonra tekrar deneyin.")
                        }
                    }
                }
                else{
                    
                    
                    
                    //Veritabanına kayıt yapıyoruz
                    self.ref.child("users").child(user!.uid).setValue(["email": self.emailField.text])
                    
                    print("************Kaydınız başarıyla Gerçekleşti ")
                    self.emailField.text = ""
                    self.passField.text = ""
                    self.passAgainField.text = ""
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    
                    let homeVC = storyboard.instantiateViewController(withIdentifier: "signInVC") as! SignInViewController
                    
                    self.present(homeVC, animated: true, completion: nil)
                }
            })
        }
    }
    
    // Klavyede Done tuşu fonksiyonu
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }


}
extension UIViewController {
    func showAlert(title: String, message: String, handler: ((UIAlertAction) -> Swift.Void)? = nil) {
        DispatchQueue.main.async { [unowned self] in
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: handler))
            self.present(alertController, animated: true, completion: nil)
        }
    }
}
