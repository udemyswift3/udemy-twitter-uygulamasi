//
//  UsersViewController.swift
//  TwitterUdemyApp
//
//  Created by kerimcaglar on 10/12/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class UsersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var usersListTableView: UITableView!
    var usersList = [NSDictionary?]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        FIRDatabase.database().reference().child("users").queryOrdered(byChild: "email").observe(.childAdded, with: { snapshot in
            
            let key = snapshot.key
            let snapshot = snapshot.value as? NSDictionary
            snapshot?.setValue(key, forKey: "uid")
            let myUserId = FIRAuth.auth()?.currentUser?.uid
            print("key:\(key) userId: \(String(describing: myUserId!))")
            //Giriş yapan kullanıcıyı bulma
            if(key == myUserId!){
                print("Giriş yapan kulllanıcı tabloda gösterilmemeli")
            }
            else{
                self.usersList.append(snapshot)
                self.usersListTableView.insertRows(at: [IndexPath(row:self.usersList.count-1,section:0)], with: UITableViewRowAnimation.automatic)
            }
        })

        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usersList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "users", for: indexPath) as! UsersTableViewCell
        let user : NSDictionary?
        user = self.usersList[indexPath.row]
        cell.userName.text = user?["fullName"] as? String
        cell.nickName.text = user?["mentionName"] as? String
        
        return cell
    }

    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
