//
//  SignInViewController.swift
//  TwitterUdemyApp
//
//  Created by Kerim Çağlar on 08/05/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import UIKit
import Firebase

class SignInViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var emailField: UITextField!
    
    @IBOutlet weak var passField: UITextField!
    
    var ref = FIRDatabase.database().reference()

    override func viewDidLoad() {
        super.viewDidLoad()

        emailField.delegate = self
        passField.delegate = self
        // Do any additional setup after loading the view.
    }

    @IBAction func signIn(_ sender: Any) {
        
        if(!(emailField.text?.isEmpty)! && !(passField.text?.isEmpty)!){
            
            // Kullanıcının email mi yoksa kullanıcı adı mı girdiğini isValidEmail fonksiyonu ile belirliyoruz.
            
            if isValidEmail(testStr: emailField.text!) == false {
                
                // Kullanıcı mentionName ile giriş yapmışsa, böyle bir kullanıcı adının olup olmadığını kontrol ediyoruz.
                
                self.ref.child("mentionName").child(emailField.text!).observeSingleEvent(of: .value, with: { (snapshot) in
                    
                    if snapshot.exists() == true {
                   
                        // Kullanıcı adı doğruysa, kullanıcı adının ait olduğu UserID karşılığını çekiyoruz.
                        
                        let uid = snapshot.value as! String
                        
                        print(uid)
                        // Her ihtimale karşı UserID'nin çekilip çekilmediğini kontrol ediyoruz.
                        
                        if uid.isEmpty != true {
                            // UserID başarılı bir şekilde çekilmişse users tablomuzda giriş yapmaya çalışan kullanıcnın emailini alıyoruz.
                            self.ref.child("users").child(uid).child("email").observeSingleEvent(of: .value, with: { (snapshot) in
                                
                                let email = snapshot.value as! String
                                print(email)
                                
                                print(snapshot)
                                // Yine her ihtimale karşı Emailin çekilip çekilmediğini kontrol ediyoruz.
                                if email.isEmpty == true {
                                    self.showAlert(title: "", message: "Bir şeyler yanlış gitti.")
                                }
                                else {
                                    // Son olarak çekilen email ile kullanıcıya giriş yaptırıyoruz.
                                    
                                    self.signInwithEmail(email: email, password: self.passField.text!)
                                    UserDefaults.standard.set(email, forKey: "email")
                                    UserDefaults.standard.set(self.passField.text, forKey: "password")
                                }
                                
                            
                        
                    })
                    
                    
                        }

                        
                        
                    }
                    else
                    {
                        // Öyle bir kullanıcı adı yoksa, güvenlik sebebiyle uyarıyı aşağıdaki gibi gösteriyoruz..
                        self.showAlert(title: "", message: "Yanlış bir email veya kullanıcı adı girdiniz.")
                    }
                    
                })}
                
            else
            {
                 signInwithEmail(email: emailField.text!, password: passField.text!)
                UserDefaults.standard.set(self.emailField.text, forKey: "email")
                UserDefaults.standard.set(self.passField.text, forKey: "password")
            }
            
            
           
            
        }
            
            
            
        
            	        else{
            print("Lütfen boş alan bırakmayınız")
            showAlert(title: "", message: "Lütfen boş alan bırakmayınız.")
        }
    }
    
    // Klavyede Done tuşu fonksiyonu
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func signInwithEmail(email: String, password: String) {
        
        FIRAuth.auth()?.signIn(withEmail: email, password: password, completion: { (user, error) in
            if(error != nil)
            {
                if let HataKodu = FIRAuthErrorCode(rawValue: error!._code) {
                    switch HataKodu {
                    case .errorCodeWrongPassword:
                        self.showAlert(title: "", message: "Hatalı email veya şifre girdiniz.")
                    case .errorCodeInvalidEmail:
                        self.showAlert(title: "", message: "Hatalı email veya şifre girdiniz.")
                    // Güvenlik açısından hangisinin yanlış hangisinin doğru girildiği kullanıcıya gösterilmez.
                    case .errorCodeNetworkError:
                        self.showAlert(title: "", message: "İnternet bağlantınızda bir hata saptandı.")
                    // Yukarıdakilerin dışındaki bir hata için yapılacak default işlem;
                    default:
                        self.showAlert(title: "", message: "Bir hata oluştu lütfen daha sonra tekrar deneyin. ")
                    }
                }
                
            }
            else
            {
                
                // Giriş yapmış kullancının ID'sini alıyoruz.
                
                let uid = FIRAuth.auth()?.currentUser?.uid
                
                // Kullanıcı ID'sini kullanarak Firebase Database tablomuzda "mentionName" keyine karşılık gelen bir değer olup olmadığını sorguluyoruz.
                
                self.ref.child("users").child(uid!).observeSingleEvent(of: .value, with: { (snapshot) in
                    let value = snapshot.value as? NSDictionary
                    let mentionName = value?["mentionName"] as? String ?? ""
                    print(mentionName)
                    
                    if mentionName == "" {
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        
                        let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeViewController
                        
                        self.present(homeVC, animated: true, completion: nil)
                    }
                    else {
                        
                        let TwitterHomeVC = self.storyboard?.instantiateViewController(withIdentifier: "TwitterHomeVC")
                        self.present(TwitterHomeVC!, animated: true, completion: nil)
                    }
                }
                    
                    
                
                )}})
            
    }

    func isValidEmail(testStr:String) -> Bool {
        
        print("validate emailId: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
        
    }

}

    





