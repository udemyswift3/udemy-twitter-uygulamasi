//
//  NewTweetViewController.swift
//  TwitterUdemyApp
//
//  Created by Kerim Çağlar on 11/06/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import UIKit
import Firebase

class NewTweetViewController: UIViewController,UITextFieldDelegate,UITextViewDelegate {
    
    @IBOutlet weak var textView: UITextView!
    
    var ref = FIRDatabase.database().reference()
    var user = FIRAuth.auth()?.currentUser
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        textView.text = "Neler Oluyor?"
        textView.textColor = UIColor.lightGray
    }
    
    
    @IBAction func cancelButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(textView.textColor == UIColor.lightGray){
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    @IBAction func sendTweet(_ sender: Any) {
        
        if(textView.text.characters.count>0){
            
            let uniqeId = ref.child("tweets").childByAutoId().key
            
//            let date = String(Int((NSDate().timeIntervalSince1970)))
//            
//            print("date: \(date)")
//            
//            ref.child("tweets").child(user!.uid).child(uniqeId).child(date).child("tweet").setValue(textView.text)
            
            let childUpdates = ["/tweets/\(self.user!.uid)/\(uniqeId)/tweet":textView.text,
                                "/tweets/\(self.user!.uid)/\(uniqeId)/tarih":"\(Date().timeIntervalSince1970)"] as [String : Any]
            
            self.ref.updateChildValues(childUpdates)
            

            dismiss(animated: true, completion: nil)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
}
