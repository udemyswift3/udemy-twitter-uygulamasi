//
//  UsersTableViewCell.swift
//  TwitterUdemyApp
//
//  Created by kerimcaglar on 10/12/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import UIKit

class UsersTableViewCell: UITableViewCell {
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var nickName: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
